# eagle on linux

## Known issues
Eagle (Easily Applicable Graphical Layout Editor) on Linux is 32 bit application and it isnt possible to run on plain 64 bit OS. Thouse day more and more  Linux distribution by default support onl 64 bit library.
On same time Eagle is stronfly connected to libssl in version 1.0.0.

## Possible solution
Follow solution is base for follow configuration :
*  Linux CentOS 8 
*  Eagle 7.2.0
*  yum use standard and Epel repo

It may work for other Linux distribution, but probably need some improvment on package system command and package name.

First we need to check if we have support for glibc.i686 package :
```
[root@localhost ~]# rpm -qa | grep glibc | grep i686
glibc-2.28-72.el8.i686
glibc-devel-2.28-72.el8.i686
glibc-headers-2.28-72.el8.i686
```

Be aware that ldd command wouldnt work on binary file (bin/eagle) if no support for 32bit software isnt install. Also if you try to run binnary file without support system (bash) will return warrning that file doesnt exist.

Installation command for most packaga needed for eagle are like follow :
```
yum install glibc.i686 glibc-devel.i686
yum install freetype.i686 freetype-devel.i686
yum install libXrender-devel.i686 libXrender.i686
yum install libXrandr.i686 libXrandr-devel.i686
yum install libXcursor-devel.i686 libXcursor.i686
yum install fontconfig.i686 fontconfig-devel.i686
yum install libgcc.i686
yum install libXi.i686 libXi-devel.i686
```

After previuse commands eagle need only libssl and libcrypto library. However it need versio 1.0.x , any other will return some warrning and errors.
Install openssl is relativly ease if you have set development environment, just follow nex command:
* like ordinary user 
```
git clone -b OpenSSL_1_0_2-stable https://github.com/openssl/openssl
cd openssl
setarch i386 ./config -m32 shared
make
make test
su
cp libcrypto.so.1.0.0 /lib
cp libssl.so.1.0.0 /lib
```

After all that steps eagle binanry is ready to run on CentOS 8 .